FROM node:10.13-alpine as react-build
ENV NODE_ENV production
WORKDIR /app
COPY ["package.json", "*lock", "./"]
RUN yarn install

COPY . ./
RUN yarn build

FROM nginx:alpine
COPY docker/nginx/default.conf /etc/nginx/conf.d/default.conf
COPY --from=react-build /app/build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
