import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './components/App';
import * as serviceWorker from './serviceWorker';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import reducers from './reducers/index';
import thunk from 'redux-thunk';
import topbar from 'topbar';
topbar.config({
  autoRun: true,
  barThickness: 3,
  barColors: {
    '0': 'rgba(26,  188, 156, .9)',
    '.25': 'rgba(52,  152, 219, .9)',
    '.50': 'rgba(241, 196, 15,  .9)',
    '.75': 'rgba(230, 126, 34,  .9)',
    '1.0': 'rgba(211, 84,  0,   .9)'
  },
  shadowBlur: 10,
  shadowColor: 'rgba(0,   0,   0,   .6)'
})
const store = createStore(reducers,
  applyMiddleware(thunk))

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>, document.getElementById('root'));

serviceWorker.unregister();
