import * as Activate from '../constants/index';
import { callApi } from './CallAPI';
export const show = () => ({
    type: Activate.SHOW
})
export const actFetchProductsRequest = () => {

    return (dispatch) => {
        return callApi('exam?limit=100', 'GET', null).then(res => {
            return dispatch(actFetchExams(res.data.data))
        })
    };

}

export const actFetchExams = (exams) => {

    return {
        type: Activate.SHOW_EXAM,
        exams
    }
}

export const actGetExamToNRequest = (n) => {

    return dispatch => {
        return callApi(`exam?level=${n}&limit=2`, 'GET', null).then(res => {
            //console.log(res.data.data);
            return dispatch(actGetExamtoN(res.data.data))
        })
    }
}
export const actGetExamtoN = (exams) => {
    return {
        type: Activate.EXAM_TO_N,
        exams
    }
}


// export const actLoadShikenSever = (id) => {
//     return dispatch => {
//         console.log(id);

//     }
// }

export const actCheckLogin = () => {
    return {
        type: Activate.ISAUTH
    }
}
