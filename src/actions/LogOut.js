import * as Activate from '../constants/index'
import { callApi } from './CallAPI'

export const actLogOutSuccess = (result) => ({
    type: Activate.LOGOUT_SUCCESS,
    result
})
export const actCheckPublisher = () => ({
    type: Activate.IS_PUBLISHER
})

export const actSignOutServer = () => {
    return dispatch => {
        callApi('account/logout', 'POST', null)
            .then(res => {
                console.log(res.data)
                dispatch(actLogOutSuccess(res))
            })
            .then(res => {
                dispatch(actCheckPublisher())
            })
    }
}
