import * as Action from 'constants/index'
import { callApiWithAuthentication } from './CallAPI'
import { actGetExam } from './Exam'
import * as ConstantCode from 'constants/constant-code'
import * as Utils from 'helpers/Utils'

export const actStartExamTest = (payload, status, answerSheet, result) => ({
    type: Action.START_EXAM_TEST,
    payload,
    status,
    answerSheet,
    result
})

export const actEndExamTest = () => ({
    type: Action.END_EXAM_TEST
})

export const actSetExamTestStatus = (status) => {

    localStorage.setItem('examTestStatus', JSON.stringify(status))

    return {
        type: Action.SET_EXAM_TEST_STATUS,
        status
    }
}

export const actPushExamTestResult = (result) => ({
    type: Action.PUSH_EXAM_TEST_RESULT,
    result
})

export const actSetAnswerSheet = (answerSheet) => {

    localStorage.setItem('examTestAnswerSheet', JSON.stringify(answerSheet))

    return {
        type: Action.SET_EXAM_TEST_ANSWER_SHEET,
        answerSheet
    }
}

const extractChooseByPart = (part) => {
    let result = [];

    for (let mondai of part.mondais)
        for (let sentence of mondai.sentence)
            for (let question of sentence.question) {
                result.push({
                    'question_id': question.id,
                    'choose_id': null
                })
            }

    return result;
}

const extractInitialAnswerSheet = (exam) => {
    return {
        part1: extractChooseByPart(exam.parts[0]),
        part2: extractChooseByPart(exam.parts[1]),
        part3: extractChooseByPart(exam.parts[2]),
    }
}

export const actStartExamRequest = (id) => {
    return dispatch => {

        function getAndParseLocalStorageItem(key) {
            return localStorage.getItem(key) ? JSON.parse(localStorage.getItem(key)) : null
        }

        let examTestChecker = getAndParseLocalStorageItem('examTest')

        if (examTestChecker && examTestChecker.exam_id !== id) {
            Utils.removeItemsInLocalStorage('examTest', 'examTestStatus', 'examTestAnswerSheet', 'examTestResult')
        }

        let examTest = getAndParseLocalStorageItem('examTest')
        let examTestStatus = getAndParseLocalStorageItem('examTestStatus')    
        let examTestAnswerSheet = getAndParseLocalStorageItem('examTestAnswerSheet')
        let examTestResult = getAndParseLocalStorageItem('examTestResult')

        let examData

        function getTotalDurationOfExam(exam) {
            let parts = exam.parts
            let totalDuration = 0

            for (let part of parts) {
                totalDuration += exam.examDurationSetting[`${part.part}_duration`]
            }

            return totalDuration
        }

        function isExamTestExpired(examTest, exam) {
            function diff_minutes(date1, date2) {
                let diff =(date2.getTime() - date1.getTime()) / 1000;
                diff /= 60;
                return Math.abs(Math.round(diff));
            }

            let totalDuration = getTotalDurationOfExam(exam)    
            
            let updatedAtExamTest = new Date(examTest.updated_at)
            let now = new Date(Date.now())
    
            let diffMins = diff_minutes(now, updatedAtExamTest)
      
            return diffMins > totalDuration
        }

        return callApiWithAuthentication(`exam/${id}`, 'GET', null)
            .then(res => {
                console.log("Res exam data in actStartExamRequest: ", res.data.data)
                dispatch(actGetExam(res.data.data))
                examData = res.data.data

                if (!examTest || !examTestAnswerSheet || !examTestResult
                    || !examTestStatus || examTestStatus.currentPart === ConstantCode.EXAM_TEST_PART_CODE.END)
                    return callApiWithAuthentication(`exam/${id}/submit`, 'POST', null)
                else
                    return null
            })
            .then(res => {
                console.log("Res chain: ", res)
                if (res) {
                    console.log("Res exam test in actStartExamRequest: ", res.data.data)
                    let examTest = res.data.data

                    localStorage.setItem('examTest', JSON.stringify(examTest))

                    let examTestStatus = {
                        currentPart: ConstantCode.EXAM_TEST_PART_CODE.PART1,
                        secondLeft: examData.examDurationSetting[`part${ConstantCode.EXAM_TEST_PART_CODE.PART1}_duration`]
                            * 60,
                    }

                    localStorage.setItem('examTestStatus', JSON.stringify(examTestStatus))

                    let examTestAnswerSheet = extractInitialAnswerSheet(examData)
                    localStorage.setItem('examTestAnswerSheet', JSON.stringify(examTestAnswerSheet))

                    let examTestResult = []
                    localStorage.setItem('examTestResult', JSON.stringify(examTestResult))

                    dispatch(actStartExamTest(examTest, examTestStatus, examTestAnswerSheet, examTestResult))

                }
                else {
                    dispatch(actStartExamTest(examTest, examTestStatus, examTestAnswerSheet, examTestResult))
                }
            })

    }
}

export const actSubmitExamByPartRequest = (exam, examTest) => {

    return dispatch => {
        let body = {
            submitted_exam_id: examTest.payload.id,
            chooses: examTest.answerSheet[`part${examTest.status.currentPart}`]
        }
        console.log("Log body before submit part: ", body)

        return callApiWithAuthentication(`exam/part/submit?part=part${examTest.status.currentPart}`, 'POST', body).then(res => {
            console.log("Exam in actSubmitExamByPartRequest: ", exam)
            console.log("ExamTest in actSubmitExamByPartRequest: ", examTest)
            dispatch(actSetExamTestStatus({
                currentPart: examTest.status.currentPart + 1,
                secondLeft: exam.examDurationSetting[`part${examTest.status.currentPart + 1}_duration`] * 60
            }))

            console.log("Data submit part: ", res.data.data)
            dispatch(actPushExamTestResult(res.data.data))
        }).catch(err => {
            console.log('Error actSubmitExamByPartRequest : ', err.message)
        })
    }
}

export const actCheckAnAnswer = (answerSheet, part, question_id, choose_id) => {
    return dispatch => {
        console.log('answerSheet: ', answerSheet)
        console.log('part: ', part)
        console.log('question_id: ', question_id)
        console.log('choose_id: ', choose_id)

        let answerSheetPart = answerSheet[`part${part}`];

        let questionChooseObject = answerSheetPart.find((object) => {
            return object.question_id === question_id
        })

        questionChooseObject.choose_id = choose_id

        dispatch(actSetAnswerSheet(answerSheet));
    }
}