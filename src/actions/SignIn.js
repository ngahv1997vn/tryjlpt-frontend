import * as Activate from '../constants/index'
import { callApi } from './CallAPI'
export const actSignInSuccess = (result) => ({
    type: Activate.SIGNIN_SUCCESS,
    result
})

export const actOnSignIn = () => ({
    type: Activate.ON_SIGNIN
})

export const actSetRedirectTo = (redirectTo) => ({
    type: Activate.SIGNIN_SET_REDIRECT,
    redirectTo
})

export const actSignInError = (result) => ({
    type: Activate.SIGNIN_ERROR,
    result
})

export const actCheckPublisher = () => ({
    type: Activate.IS_PUBLISHER
})

export const actSignInServer = (user) => {
    return dispatch => {
        // initiate loading state  
        dispatch(actOnSignIn())
        // Action login
        callApi('account/login', 'POST', user)
            .then(res => {
                dispatch(actSignInSuccess(res))
            })
            .then(res => {
                dispatch(actCheckPublisher())
            })
            .catch(err => {
                dispatch(actSignInError(err))
            })
    }
}


