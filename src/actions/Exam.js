import * as Activate from '../constants/index'
import { callApiWithAuthentication, callApi } from './CallAPI'

export const actCreateExamSuccess = (result) => ({
    type: Activate.CREATE_EXAM_SUCCESS,
    result
})

export const actCreateExam = () => ({
    type: Activate.CREATE_EXAM
})

export const actCreateExamError = (result) => ({
    type: Activate.CREATE_EXAM_ERROR,
    result
})

export const actGetMondaiByLevel = (result) => ({
    type: Activate.GET_MONDAI_BY_LEVEL,
    result
})

export const actCreateExamServer = (data) => {
    return dispatch => {
        // initiate loading state  
        dispatch(actCreateExam())
        // Action create exam
        data.is_private = 0
        data = {
            ...data,
            exam_duration_setting_id: 1,
            template: data.level,
            publishing_status: "temp",
            part1_score: 60,
            part2_score: 60,
            part3_score: 60
        }
        callApiWithAuthentication('exam/create', 'POST', data)
            .then(res => {
                dispatch(actCreateExamSuccess(res.data))
                return res.data
            })
            .then(res => {
                callApiWithAuthentication(`mondai/${res.data.level ? res.data.level : 'N5'}/get`)
                    .then(result => {
                        dispatch(actGetMondaiByLevel(result.data))
                    })
                    .catch(error => dispatch(actCreateExamError(error)))
            })
            .catch(err => {
                alert(err.message)
                dispatch(actCreateExamError(err))
            })
    }
}


// sua lai
export const actShowExamUserCreateRequest = () => {
    return dispatch => {
        return callApiWithAuthentication('exam/me', 'GET', null).then(res => {
            dispatch(actShowExamUserCreate(res.data.data))
        })
    }
}

export const actShowExamUserCreate = (exams) => {
    return {
        type: Activate.SHOW_EXAM_OF_USER,
        exams
    }
}

export const actOnCreateExam = () => {
    return {
        type: Activate.ON_CREATE_EXAM_LOCAL
    }
}

export const actOnCreateExamLocal = () => {
    return dispatch => {
        dispatch(actOnCreateExam())
    }
}

export const actUpdateExamPart1 = (exam) => {
    return {
        type: Activate.UPDATE_EXAM_PART1,
        exam
    }
}

export const actUpdateExamPartAPI = (exam, idExam) => {
    return dispatch => {
        dispatch(actCreateExam())
        return callApiWithAuthentication(`exam/${idExam}/sentence/update`, 'POST', exam).then(res => {
            dispatch(actUpdateExamPart1(res))
        }).catch(error => {
            dispatch(actCreateExamError(error))
        })
    }
}

export const actUpdateExamPart2 = (exam) => {
    return {
        type: Activate.UPDATE_EXAM_PART2,
        exam
    }
}

export const actUpdateExamPartAPI2 = (exam, idExam) => {
    return dispatch => {
        dispatch(actCreateExam())
        return callApiWithAuthentication(`exam/${idExam}/sentence/update`, 'POST', exam).then(res => {
            dispatch(actUpdateExamPart2(res))
        }).catch(error => {
            dispatch(actCreateExamError(error))
        })
    }
}
//delete exam by id
export const actDeleteExamRequest = (id) => {
    return dispatch => {
        console.log(id);
        return callApiWithAuthentication(`exam/${id}/delete`, 'DELETE', null).then(res => {
            dispatch(actDeleteExam(id))
        }).then(res => {
            dispatch(actShowExamUserCreateRequest())
        }).catch(res => {
            dispatch(actCreateExamError(res))
        })
    }
}

export const actDeleteExam = (id) => {
    return {
        type: Activate.DELETE_EXAM_ID,
        id
    }
}

export const actGetExamRequest = (id) => {
    return dispatch => {
        dispatch(actCreateExam())
        return callApiWithAuthentication(`exam/${id}/sentence/update`, 'GET', null).then(res => {
            console.log(res.data.data);
            dispatch(actGetExam(res.data.data))
            return res.data
        }).then(res => {
            console.log(res.data);

            callApiWithAuthentication(`mondai/${res.data.template ? res.data.template : 'N5'}/get`)
                .then(result => {
                    dispatch(actGetMondaiByLevel(result.data))
                })
                .catch(error => dispatch(actCreateExamError(error)))
        })
    }
}

export const actGetExam = (exam) => {
    return {
        type: Activate.GET_EXAM_ID,
        exam
    }
}

export const actGetExamDetailAPI = (id) => {
    return dispatch => {
        dispatch(actCreateExam())
        return callApi(`exam/${id}`, 'GET', null).then(res => {
            dispatch(actGetExamDetail(res.data.data));
        }).catch(error => {
            dispatch(actCreateExamError(error.message))
        })
    }
}

export const actGetExamDetail = (exam) => {
    return {
        type: Activate.GET_EXAM_DETAIL,
        exam
    }
}
