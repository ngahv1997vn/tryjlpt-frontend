import axios from 'axios';
import * as Config from '../constants/config'

export const callApi = (endpoint, method = 'GET', body) => {
    return axios({
        method: method,
        url: `${Config.API_URL}/${endpoint}`,
        data: body
    })
}

export const callApiWithAuthentication = (endpoint, method = 'GET', body) => {
    let jwt = localStorage.getItem("token");
    return axios({
        method: method,
        url: `${Config.API_URL}/${endpoint}`,
        headers: {
            'Authorization': `Bearer ${jwt}`
        },
        data: body
    })
}

