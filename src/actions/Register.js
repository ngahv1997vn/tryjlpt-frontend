import * as Activate from '../constants/index'
import { callApi } from './CallAPI'
export const actSignUp = () => ({
    type: Activate.SIGNUP
})
export const actSignUpErr = (result) => ({
    type: Activate.SIGNUP_ERROR,
    result
})
export const actSignUpSuccess = () => ({
    type: Activate.SIGNUP_SUCCESS
})
export const actSignUpInit = () => ({
    type: Activate.INIT_SIGNUP
})
export const actSignUpServer = (user) => {
    console.log(user);

    return dispatch => {
        dispatch(actSignUp());
        return callApi('account/register', 'POST', user).then(res => {
            dispatch(actSignUpSuccess())

        }).catch(err => {
            console.log(err);
            let errors = err.response.data.meta.errors.username;
            // console.log(errors);

            dispatch(actSignUpErr(errors))
        })
    }
}