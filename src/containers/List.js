import { connect } from 'react-redux'
import List from '../components/List'

const mapStateToProps = state => ({
    list: state.show
})

export default connect(mapStateToProps, null)(List);