import React, { Component } from 'react';
import HeaderIn from './../shared/Header'
import RowInput from '../shared/RowInput';
import { connect } from 'react-redux';
import { actSignUpServer, actSignUpInit } from '../../actions/Register';
import swal from "sweetalert";
import { Redirect } from 'react-router-dom';
import LoadingComponent from '../shared/Loading';

class SignUp extends Component {

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            confirmPassword: '',
        }
    }

    submitForm = (e) => {
        e.preventDefault()
        let { username, password, checkCF, checkN, checkP } = this.state;
        let user = { username, password }
        if (username !== "") {
            if (username.length < 5) {
                this.setState({
                    checkN: "5 characters or more"
                });
            }

        }
        if (password !== "") {
            if (password.length < 6) {
                this.setState({
                    checkP: "6 characters or more"
                });
            }
        }

        if (checkCF === "" && checkP === "" && checkN === "") {
            this.props.signUp(user);
            // if(this.props.isRedirect=)

        } else {
            swal("Please enter correctly!");
        }
    }

    componentDidMount() {
        this.props.init()
    }

    onChange = (e) => {
        const name = e.target.name;
        const value = e.target.value;
        if (e.target.name === "password") {
            if (e.target.value === this.state.confirmPassword) {
                this.setState({
                    checkCF: ""
                });
            } else {
                this.setState({
                    checkCF: "Did not match"
                });
            }
        }
        if (e.target.name === "confirmPassword") {
            if (e.target.value === this.state.password) {
                this.setState({
                    checkCF: ""
                });
            } else {
                this.setState({
                    checkCF: "Did not match"
                });
            }
        }
        if (e.target.name === "username") {
            if (e.target.value === "") {
                this.setState({
                    checkN: "not empty"
                });
            } else {
                this.setState({
                    checkN: ""
                });
            }
        }
        if (e.target.name === "password") {
            if (e.target.value === "") {
                this.setState({
                    checkP: "not empty"
                });
            }
            else {
                this.setState({
                    checkP: ""
                });
            }
        }

        this.setState({
            [name]: value
        });
    }

    render() {
        let { username, password, confirmPassword } = this.state;
        let { signup } = this.props

        return (
            <div>
                <div className="container img-thumbnail" style={{ margin: '30px auto 30px auto', padding: '10px 20px 30px 20px', height: "380px" }}>
                    <div className="row" >
                        <div className="col-md-12">
                            <form className="form-horizontal" onSubmit={this.submitForm}>
                                <HeaderIn title="Register New User" />
                                <RowInput
                                    title={"Username"}
                                    icon={"fa-user"}
                                    name={"username"}
                                    value={username}
                                    onChange={(e) => this.onChange(e)}
                                    type={"text"}
                                    error={this.state.checkN}
                                    disabled={signup.isLoading}
                                />
                                <RowInput
                                    name={"password"}
                                    value={password}
                                    title={"Password"} icon={"fa-key"}
                                    onChange={(e) => this.onChange(e)}
                                    type={"password"}
                                    error={this.state.checkP}
                                    disabled={signup.isLoading}
                                />
                                <RowInput
                                    name={"confirmPassword"}
                                    value={confirmPassword}
                                    title={"Confirm Password"} icon={"fa-key"}
                                    onChange={(e) => this.onChange(e)}
                                    type={"password"}
                                    error={this.state.checkCF}
                                    disabled={signup.isLoading}
                                />
                                <div className="row">
                                    <div className="col-md-3" />
                                    <div className="col-md-6">
                                        <button type="submit"
                                            className="btn btn-success"
                                            onClick={this.onClick}
                                        ><i className="fa fa-user-plus" /> Register</button>
                                        <LoadingComponent isLoading={signup.isLoading} />
                                    </div>
                                </div>

                                {Object.keys(signup.payload).length ? <Redirect to="/" /> : ""}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        err: state.signup,
        signup: state.signup,
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        signUp: (user) =>
            dispatch(actSignUpServer(user)),
        init: () =>
            dispatch(actSignUpInit())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);