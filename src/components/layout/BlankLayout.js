import React, { Component } from 'react';
import { Route } from "react-router-dom";
import SignIn from '../signin/SignIn';

const BlankLayout = ({children, ...rest}) => {
    return (
      <Route {...rest} render={matchProps => (
        <div>
          { React.Children.map(children, (child) => React.cloneElement(child, { ...matchProps }) )}
        </div>
      )} />
    )
  };

export default BlankLayout;