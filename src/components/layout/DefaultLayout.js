import React, { Component } from 'react';
import { Route } from "react-router-dom";
import Header from '../header/Header';
import Footer from '../footer/Footer';
import SignIn from '../signin/SignIn';

const DefaultLayout = ({children, ...rest}) => {
    return (
      <Route {...rest} render={matchProps => (
        <div>
            <Header />
            { React.Children.map(children, (child) => React.cloneElement(child, { ...matchProps }) ) }
            <Footer />
            <SignIn />
        </div>
      )} />
    )
  };

export default DefaultLayout;