import React, { Component } from 'react';

class InputRequiredBelow extends Component {
    render() {
        let { children, title, required } = this.props;
        return (
            <div className="row">
                <div className="col-md-3 field-label-responsive">
                    <label htmlFor="name">{title}</label>
                </div>
                <div className="col-md-8">
                    <div className="form-group">
                        <div className="input-group mb-2 mr-sm-2 mb-sm-0">
                            {children}
                        </div>
                        <span className="text-danger align-middle">
                            {required}
                        </span>
                    </div>
                </div>
            </div>

        );
    }
}

export default InputRequiredBelow;