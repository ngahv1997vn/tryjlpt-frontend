import React, { Component } from 'react';

class AvataItem extends Component {
    render() {
        let { color } = this.props
        return (
            <div className="ava-item">
                <div title="Dương Ngọc Huân" className={`avatar-wrapper ${color} `}  >
                    <span>DNH</span>
                </div>
            </div>
        );
    }
}

export default AvataItem;