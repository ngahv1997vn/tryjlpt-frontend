import React, { Component } from 'react';
import { css } from '@emotion/core';
import { PulseLoader } from 'react-spinners';

class LoadingComponent extends Component {
    render() {
        let { isLoading } = this.props;
        const override = css`
        `;
        return (
            <div className='sweet-loading'>
                <PulseLoader
                    css={override}
                    sizeUnit={"px"}
                    size={15}
                    color={'#36D7B7'}
                    loading={isLoading}
                />
            </div>
        );
    }
}

export default LoadingComponent;