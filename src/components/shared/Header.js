import React, { Component } from 'react';

class Header extends Component {
    render() {
        let { title } = this.props
        return (
            <div className="row">
                <div className="col-md-12">
                    <h3>{title}</h3>
                    <hr />
                </div>
            </div>
        );
    }
}

export default Header;