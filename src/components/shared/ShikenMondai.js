import React, { Component } from 'react';
import ButtonDetail from './ButtonDetail';
import { connect } from 'react-redux';
import { actGetExamToNRequest } from '../../actions/index';
import { actGetExamRequest } from '../../actions/Exam'

class ShikenMondai extends Component {
    render() {
        let { title, children, product } = this.props
        let ClassExamLabel = product.is_free === "1" ? 'free-exam-label' : 'exam-label';
        let ExamLabel = product.is_free === "1" ? 'Free' : '';
        let dispatchActGetExamRequest = (id, level) => {
            return () => {
                this.props.getExamToN(level);
            }
        }
        let minutes = parseInt(product.part1_score) + parseInt(product.part2_score) + parseInt(product.part3_score);

        return (
            <div className="col-md-6 padding-bottom ">
                <div className='img-thumbnail'>
                    <header className="w3-container w3-light-grey">
                        <h3>{title}</h3>
                    </header>
                    <div className="exam-body ">
                        <p className="d-inline"><i className="fa fa-calendar"></i>{minutes} minutes</p>
                        <p className={`d-inline ${ClassExamLabel}`}>{ExamLabel}</p>
                        <hr />
                        {children}
                        <div className="clear-both"></div>
                        <hr />
                        {this.colorTopic(product.level)}
                    </div>
                    <ButtonDetail id={product.id} changeId={dispatchActGetExamRequest} level={product.level} />
                </div>
            </div>
        );
    }

    colorTopic = (typeTopic) => {
        if (typeTopic === "N1") {
            return <p className="d-inline exam-label n1-exam">{typeTopic}</p>
        }
        else if (typeTopic === "N2") {
            return <p className="d-inline exam-label n2-exam">{typeTopic}</p>
        }
        else if (typeTopic === "N3") {
            return <p className="d-inline exam-label n3-exam">{typeTopic}</p>
        }
        else if (typeTopic === "N4") {
            return <p className="d-inline exam-label n4-exam">{typeTopic}</p>
        }
        else if (typeTopic === "N5") {
            return <p className="d-inline exam-label n5-exam">{typeTopic}</p>
        }
    }

}
// export default ShikenMondai
const mapDispatchToProps = (dispatch) => {
    return {
        getExamToN: (n) => {
            dispatch(actGetExamToNRequest(n))
        }
    }
}
export default connect(null, mapDispatchToProps)(ShikenMondai)