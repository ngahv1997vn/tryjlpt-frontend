import React, { Component } from 'react';
import { timingSafeEqual } from 'crypto';

class SimpleCountdown extends Component {
    constructor(props) {
        super(props);

        this.state = {
            initialSeconds: props.hour * 3600 + props.minute * 60 + props.second,
            seconds: props.hour * 3600 + props.minute * 60 + props.second,
            timeOutFlag: false,
        }
    }

    componentDidMount() {

        this.interval = setInterval(() => {
            if (this.state.seconds > 0) {
                if (this.state.seconds === this.state.initialSeconds) {
                    if (typeof this.props.callbackStart === 'function') 
                        this.props.callbackStart(this.state)
                }
                else if (this.state.seconds % 60 === 0) {
                    if (typeof this.props.callbackPerMinute === 'function') 
                        this.props.callbackPerMinute(this.state)
                }
                else {  
                    if (typeof this.props.callbackPerSecond === 'function') 
                        this.props.callbackPerSecond(this.state)
                }

                this.setState((state) => ({
                    seconds: state.seconds - 1
                }))
            } 
            else {
                this.setState((state) => ({
                    timeOutFlag: true
                }));
                clearInterval(this.interval);
                if (typeof this.props.callbackTimeOut === 'function')
                    this.props.callbackTimeOut(this.state)
            }
        }, 1000);
    }

    componentWillUnmount() {
        clearInterval(this.interval)
    }

    formatTime = (value) => {
        let valueString = value.toString();
        if (valueString.length < 2)
            valueString = '0' + valueString;

        return valueString;
    }

    render() {
        // console.log("This state seconds: ", this.state.seconds);
        let hour = Math.floor(this.state.seconds / 3600);
        let minute = Math.floor((this.state.seconds - hour * 3600) / 60);
        let second = this.state.seconds - hour * 3600 - minute * 60;

        return (
            <span className={this.props.classProp}>{this.formatTime(hour)}:{this.formatTime(minute)}:{this.formatTime(second)}</span>
        )
    }
}

export default SimpleCountdown;