import React, { Component } from 'react';

class InputText extends Component {
    render() {
        let { onChange, title, type, name, value, id, error, disabled } = this.props;
        return (
            <div>
                <label htmlFor={title}>{title + ":"}</label>
                <input
                    onChange={onChange}
                    className="form-control"
                    name={name}
                    value={value}
                    id={id}
                    placeholder={title}
                    type={type}
                    disabled={disabled}
                    required />
                <span className="text-danger">{error}</span>
            </div>
        );
    }
}

export default InputText;