import React, { Component } from 'react';

class RowInput extends Component {
    render() {
        let { onChange, name, title, type, value, icon, error, disabled } = this.props;
        return (
            <div className="row">
                <div className="col-md-3 field-label-responsive">
                    <label htmlFor="username">{title}</label>
                </div>
                <div className="col-md-6">
                    <div className="form-group">
                        <div className="input-group mb-2 mr-sm-2 mb-sm-0">
                            <div className="input-group-addon" style={{ width: '2.6rem' }}><i className={"fa " + icon} /></div>
                            <input
                                id={name}
                                onChange={onChange}
                                name={name}
                                className="form-control"
                                placeholder={title}
                                type={type}
                                value={value}
                                pattern="[A-Za-z0-9]{1,20}"
                                required
                                disabled={disabled}
                            />
                        </div>
                    </div>
                </div>
                <div className="col-md-3">
                    <div className="form-control-feedback">
                        <span className="text-danger align-middle">
                            {error}
                        </span>
                    </div>
                </div>
            </div>

        );
    }
}

export default RowInput;