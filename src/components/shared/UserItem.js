import React, { Component } from 'react';

class UserItem extends Component {
    render() {
        let { stt } = this.props;
        let classNumber = ''
        switch (stt) {
            case "1":
                classNumber = 'btn-danger'
                break;
            case "2":
                classNumber = 'btn-warning'
                break;
            case "3":
                classNumber = 'btn-primary'
                break;
            default:
                classNumber = 'btn-secondary'
                break;
        }

        return (
            <div className="user-item">
                <div className="user-img">
                    <img className='img-file' src="http://dummyimage.com/229x225.jpg/5fa2dd/ffffff" alt="Smiley face" width="60" height="60" />
                </div>
                <div className="info">
                    <div className='name'>Nguyen van A</div>
                    <div className="scores">123/180 <b style={{ color: 'black' }}>Point</b></div>
                </div>
                <div className='number'>
                    <button type="button" className={`btn ${classNumber}`} style={{ borderRadius: '8px' }}>{stt}</button>
                </div>
            </div>
        );
    }
}

export default UserItem;