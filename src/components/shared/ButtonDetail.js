import React, { Component } from 'react';
import { Link } from 'react-router-dom'

class ButtonDetail extends Component {

    render() {
        let { id, changeId, level } = this.props;
        return (
            <Link className="w3-button w3-block w3-dark-grey" to={`/exam-detail/${id}`} onClick={changeId(id, level)} >+ Detail</Link>
        );
    }
}

export default ButtonDetail;