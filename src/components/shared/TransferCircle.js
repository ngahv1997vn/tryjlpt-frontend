import React, { Component } from 'react';
import { Link } from 'react-router-dom'
class TransferCircle extends Component {
    constructor(props) {
        super(props);
        this.state = {
            successc: "",
            success1: "",
            success2: "",
            success3: "",
            pathname: ""
        }
    }
    changeStatus = (pathname) => {
        switch (pathname) {
            case '/exam/create':
                this.setState({
                    successc: "success",
                    success1: "",
                    success2: "",
                    success3: "",
                })
                break;
            case '/exam/part1':
                this.setState({
                    success1: "success",
                    successc: "",
                    success2: "",
                    success3: "",
                })
                break;
            case '/exam/part2':
                this.setState({
                    success2: "success",
                    success1: "",
                    successc: "",
                    success3: "",
                })
                break;
            case '/exam/part3':
                this.setState({
                    success3: "success",
                    success1: "",
                    success2: "",
                    successc: "",
                })
                break;
            default:
                break;
        }
    }

    render() {
        let { successc, success1, success2, success3, pathname } = this.state;
        if (pathname !== this.props.pathname) {
            this.changeStatus(this.props.pathname);
            this.setState({ pathname: this.props.pathname })
        }
        return (
            <div className="row">
                <div className="col-md-2 col-xs-2" />
                <div className="col-md-8 col-xs-8">
                    <div>
                        <ul className="timeline timeline-horizontal">
                            <li className="timeline-item">
                                <Link to="/exam/create">
                                    <div
                                        className={`timeline-badge ${successc} primary`}
                                        style={{ cursor: 'pointer' }}>C</div>
                                </Link>
                            </li>
                            <li className="timeline-item">
                                <Link to="/exam/part1">
                                    <div
                                        className={`timeline-badge ${success1} primary`}
                                        style={{ cursor: 'pointer' }}>1</div>
                                </Link>
                            </li>
                            <li className="timeline-item">
                                <Link to="/exam/part2">
                                    <div
                                        className={`timeline-badge ${success2} primary`}
                                        style={{ cursor: 'pointer' }}>2</div>
                                </Link>
                            </li>
                            <li className="timeline-item">
                                <Link to="/exam/part3">
                                    <div
                                        className={`timeline-badge ${success3} primary`}
                                        style={{ cursor: 'pointer' }}>3</div>
                                </Link>
                            </li>
                            <li className="hr">
                            </li>
                        </ul>
                    </div>
                </div>
                <div className="col-md-2 col-xs-2" />
            </div>
        );
    }
}

export default TransferCircle;