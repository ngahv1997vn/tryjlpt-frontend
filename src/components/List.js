import React, { Component } from 'react';

class List extends Component {

    render() {
        let { list } = this.props;
        return (
            <div>
                {
                    list.map((item, key) => (
                        <div key={key}>
                            <p>{item.id}</p>
                            <p>{item.username}</p>
                        </div>
                    ))
                }
            </div>
        );
    }
}

export default List;