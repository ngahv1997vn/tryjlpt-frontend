import React, { Component } from 'react';
import ModalComponent from '../shared/Modal';
import InputText from '../shared/InputText';
import { Link } from "react-router-dom";
import { connect } from 'react-redux';
import { actSignInServer } from '../../actions/SignIn';
import Loading from '../shared/Loading';

class SignIn extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
        }
    }



    onChange = (e) => {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({
            [name]: value
        })
    }

    submitForm = (e) => {
        e.preventDefault();
        let { username, password } = this.state;
        this.props.signIn({ username, password });
    }

    render() {
        let { username, password } = this.state;
        let { isLoading, result } = this.props
        return (
            <ModalComponent idModal={"modal-signin"} header={"Sign in"}>
                <form onSubmit={this.submitForm}>
                    <InputText
                        id={"s-username"}
                        name={"username"}
                        title={"Username"}
                        value={username}
                        onChange={(e) => this.onChange(e)}
                        disabled={isLoading}
                    />
                    <InputText
                        id={"s-password"}
                        name={"password"}
                        title={"Password"}
                        value={password}
                        onChange={(e) => this.onChange(e)}
                        type={"password"}
                        disabled={isLoading}
                    />
                    <span className="text-danger">{result.error && result.error.message ? result.error.message : ''}</span>
                    <br />
                    <button type="submit" className="btn btn-primary" >
                        Login
                    </button>
                    <Link className={"btn btn-default"} onClick={() => { window.$('#modal-signin').modal('hide'); }} to="/register"> Register</Link>
                    {isLoading ? <Loading isLoading={this.props.isLoading} /> : ""}
                </form>
            </ModalComponent>
        );
    }
}

const mapStateToProps = state => {
    return {
        result: state.signin,
        isLoading: state.signin.isLoading
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        signIn: (user) => {
            return dispatch(actSignInServer(user))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignIn);