import React, { Component } from 'react';
import '../App.css';
import SignUp from './signup/SignUp';
import { BrowserRouter, Switch } from "react-router-dom";
import Home from './home/Home';
import CreateExam from './exam/CreateExam';
import Profile from './my-exam/Profile';
import ExamDetail from './exam/ExamDetail';
import ExamTest from './exam/ExamTest';
import { connect } from 'react-redux'
import MyExam from './my-exam/Home'
import SignIn from './signin/SignIn';
import DefaultLayout from './layout/DefaultLayout';
import BlankLayout from './layout/BlankLayout';

class App extends Component {

  render() {
    let { isPublisher, isUser } = this.props
    return (
      <BrowserRouter>
        <Switch>
          <DefaultLayout exact path="/" >
            <Home />
          </DefaultLayout>

          <DefaultLayout path="/register">
            <SignUp />
          </DefaultLayout>
          
          {isPublisher ? 
            <DefaultLayout path="/exam">
              <CreateExam />
            </DefaultLayout>
            : null}
          
          {isUser ? 
            <DefaultLayout path="/my-profile">
              <Profile />
            </DefaultLayout>
            : null}

          <DefaultLayout path="/exam-detail/:id">
            <ExamDetail />
          </DefaultLayout>

          {isPublisher ? 
            <DefaultLayout path="/my-exam">
              <MyExam />
            </DefaultLayout>
            : null}

          <BlankLayout exact path="/exam-test/:id">
            <ExamTest />
            <SignIn />
          </BlankLayout>
          
        </Switch>
      </BrowserRouter>
    );
  }
}
const mapStateToProps = (state) => ({
  isPublisher: state.signin.isPublisher,
  isUser: state.signin.isUser
})
export default connect(mapStateToProps)(App);
