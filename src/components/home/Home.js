import React, { Component } from 'react';
import ShikenMondai from '../shared/ShikenMondai';
import AvataItem from '../shared/AvataItem';
import { connect } from 'react-redux';
import { actFetchProductsRequest } from '../../actions/index'
import './Home.css';
import UserItem from '../shared/UserItem';
import topbar from 'topbar';

class Home extends Component {

    componentWillMount() {
        this.props.fetchAllProducts();
        topbar.show()
    }

    componentDidMount() {

    }

    render() {
        let { products } = this.props;
        let { isLoading } = this.props;
        if (!isLoading)
            topbar.hide()
        return (
            <div className="container">
                <div className="row" style={{ margin: '30px 0 50px 0' }}>
                    <div className="col-lg-8 ">
                        <div className="">
                            <h2><b>New posts</b></h2>
                            <hr />
                            <div className="row">

                                {isLoading ?
                                    (<div class="load-bar">
                                        <div class="bar"></div>
                                        <div class="bar"></div>
                                        <div class="bar"></div>
                                    </div>) :
                                    this.showProducts(products)}
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4">
                        <h2><b>Ranting</b></h2>
                        <hr />

                        <div className="img-thumbnail">

                            <UserItem stt='1' />
                            <UserItem stt='2' />
                            <UserItem stt='3' />
                            <UserItem stt='4' />
                            <UserItem stt="5" />
                            <UserItem stt='6' />
                        </div>
                    </div>


                </div>
            </div>
        );
    }
    showProducts = (products) => {
        let result = null;
        if (products && products.length > 0) {
            result = products.map((product, index) => {
                return (
                    <ShikenMondai title={product.title}
                        key={index}
                        minutes="110 minutes"
                        product={product}
                    >
                        <AvataItem color="color-avata1" />
                        <AvataItem color="color-avata2" />
                        <AvataItem color="color-avata3" />
                        <div className="ava-item">
                            <div className="ava-item ava-number-total" ><span className="number">+<span>{product.amount_people}</span></span></div>
                        </div>

                    </ShikenMondai>
                )
            })
        }
        return result;
    }
}
const mapStateToProps = (state) => {
    return {
        products: state.home,
        isLoading: state.home.isLoading
    }
}
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        fetchAllProducts: () => {
            dispatch(actFetchProductsRequest())
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Home);