import React, { Component } from 'react';
import InputRequiredBelow from '../shared/InputRequiredBelow';
import { connect } from 'react-redux';
import { actCreateExamServer } from '../../actions/Exam';
import { Redirect } from 'react-router-dom'
import Loading from '../shared/Loading'

class Part extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nameRequired: "",
            categoryOfExamRequired: "",
            freeOfExamRequired: "",
            isRedirect: false
        }
    }

    submitForm = (e) => {
        e.preventDefault();
        let { name, description, level, is_private, is_free, id } = e.target
        this.props.onCreateExam({ id: id.value, title: name.value, is_free: is_free.value, description: description.value, is_private: is_private.value, level: level.value })

        this.setState({ isRedirect: true })
    }

    render() {

        let { nameRequired, categoryOfExamRequired, freeOfExamRequired, isRedirect } = this.state;
        let { exam, isLoading } = this.props
        exam = exam ? exam : {}
        if (Object.keys(exam).length && !isLoading && isRedirect) {
            return <Redirect to="/exam/part1" />
        }
        console.log(exam);

        return (
            <form onSubmit={this.submitForm}>
                <InputRequiredBelow title="Name of exam" required={nameRequired} >
                    <input type="text" name="id" defaultValue={exam.id ? exam.id : ''} hidden />
                    <input
                        type="text"
                        name="name"
                        className="form-control"
                        id="name"
                        placeholder="Name of exam"
                        required
                        defaultValue={exam.title ? exam.title : ''}
                    />
                </InputRequiredBelow>
                <InputRequiredBelow title="Description" >
                    <textarea
                        name="description"
                        className="form-control"
                        rows="5" id="comment"
                        defaultValue={exam.description ? exam.description : ''}
                    ></textarea>
                </InputRequiredBelow>
                <InputRequiredBelow >
                    <div>
                        <label><i className="fa fa-public" /> <input type="radio" name="is_private" id="public" defaultChecked />
                            Public</label>
                        <label><i className="fa fa-private" /> <input type="radio" name="is_private" id="private" />
                            Private</label>
                    </div>

                </InputRequiredBelow>
                <InputRequiredBelow title="Category of exam" required={categoryOfExamRequired}>
                    <select className="form-control" name="level" required defaultValue={exam.level ? exam.level : ''}>
                        <option value="">Choose an item</option>
                        <option value="N1">N1</option>
                        <option value="N2">N2</option>
                        <option value="N3">N3</option>
                        <option value="N4">N4</option>
                        <option value="N5">N5</option>
                    </select>

                </InputRequiredBelow>
                <InputRequiredBelow title="Fee of exam" required={freeOfExamRequired} >
                    <select className="form-control" name="is_free" required defaultValue={exam.is_free ? exam.is_free : ''} >
                        <option value="" >Choose an item</option>
                        <option value="1">Free</option>
                        <option value="0">Cost</option>
                    </select>
                </InputRequiredBelow>
                <div className="row">
                    <div className="col-md-3"></div>
                    <div className="col-md-6">
                        <button type="submit" disabled={isLoading} className="btn btn-success" ><i className="fa fa-arrow-right"></i>
                            Next</button>
                        <Loading isLoading={isLoading} />
                    </div>
                </div>
            </form>
        );
    }
}

const mapStateToProps = (state) => ({
    exam: state.exam.payload,
    isLoading: state.exam.isLoading,
    isPart: state.exam
})

const mapDispatchToProps = (dispatch, props) => {
    return {
        onCreateExam: (data) => {
            return dispatch(actCreateExamServer(data))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Part);
