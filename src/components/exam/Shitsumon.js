import React, { Component } from 'react';

class Shitsumon extends Component {
    render() {
        let { required, number } = this.props
        return (
            <div className="row">
                <div className="col-md-2">
                    <label htmlFor="sentense1">Question {number}</label>
                </div>
                <div className="col-md-10">
                    <textarea name="sentense1" rows={3} className="form-control" defaultValue={""} />
                    <span className="text-danger">{required}</span>
                </div>
            </div>
        );
    }
}

export default Shitsumon;