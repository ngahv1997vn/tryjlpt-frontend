import React, { Component } from 'react';
import { connect } from 'react-redux';
import { actCheckAnAnswer } from '../../../actions/ExamTest'
import ExamTestAnswer from './ExamTestAnswer';

class ExamTestQuestion extends Component {
    constructor(props) {
        super(props)
        this.state = {
            checkedAnswerId: this.getCheckedAnswerId(
                this.props.examTest.answerSheet,
                this.props.examTest.status.currentPart,
                this.props.questionId
            )
        }
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        if (nextProps.examTest.status.currentPart !== this.props.examTest.status.currentPart) {
            this.setState({ 
                checkedAnswerId: this.getCheckedAnswerId(
                    nextProps.examTest.answerSheet, 
                    nextProps.examTest.status.currentPart, 
                    nextProps.questionId
                ) 
            })
        }
    }

    getCheckedAnswerId(answerSheet, part, questionId) {
        let question_choose_object = answerSheet[`part${part}`].find((object) => {
            return object.question_id === questionId
        })

        return question_choose_object ? question_choose_object.choose_id : null
    }

    render() {
        return (
            <div className="exam-test-question-item">
                <div className="exam-test-question-item-question">
                    <span className="exam-test-question-item-num">
                        {this.props.questionNumber}
                    </span>
                    <span dangerouslySetInnerHTML={{ __html: this.props.questionTitle }}>

                    </span>
                </div>
                <div className="exam-test-question-item-answers">
                    {this.props.answers.map((answer, index) => {
                        return <ExamTestAnswer key={answer.id} answerId={answer.id} answerNumber={index + 1} answer={answer} parentQuestion={this} />
                    })}
                </div>

            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        examTest: state.examTest,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        checkAnAnswer: (answerSheet, part, question_id, choose_id) => {
            dispatch(actCheckAnAnswer(answerSheet, part, question_id, choose_id))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ExamTestQuestion);