import React, { Component } from 'react';
import ExamTestQuestion from './ExamTestQuestion';

class ExamTestMondai extends Component {

    render() {
        console.log("Mondai questions in render ExamTestMondai: ", this.props.mondai.sentence)
        let questionNumberIndex = 1;
        return (
            <div className="exam-test-mondai">
                <h3 className="exam-test-mondai-title" >
                    もんだい {this.props.mondaiNumber}:
                </h3>
                <div className="exam-test-mondai-content">
                    {
                        this.props.mondai.sentence.map((sentence, index) => {

                            return (
                                <div key={sentence.id} className="exam-test-sentence-item">
                                    {sentence.content && sentence.content !== "" ?
                                        (<div className="exam-test-sentence-common-question"
                                            dangerouslySetInnerHTML={{ __html: sentence.content }}>

                                        </div>) :
                                        ''
                                    }
                                    {sentence.question.map((question, index) => {
                                        return (
                                            <ExamTestQuestion key={question.id} questionId={question.id} questionNumber={questionNumberIndex++} questionTitle={question.content}
                                                answers={question.choose} />
                                        );
                                    })}
                                </div>
                            );
                        })
                    }


                </div>
            </div>
        );
    }
}

export default ExamTestMondai;