import React, { Component } from 'react'
import ExamTestDetailLanguagePart from './ExamTestDetailLanguagePart'
import ExamTestDetailSubPart from './ExamTestDetailSubPart'
import PerformExamTest from './PerformExamTest'
import * as ConstantCode from '../../../constants/constant-code'

class ExamTestDetail extends Component {

    render() {
        console.log("This props in render ExamTestDetail: ", this.props)
        return (
            <div id="fixed-exam-detail-block" className="status-exam-perform-dt hidden-xs">
                <div className="title-exam-perform">
                    <h4 >{this.props.title}</h4>
                </div>
                <div className="timeline-countdown">
                    <div className="wp-timeline">
                        <div className="timeline-detail-exam">
                            <h5 className="exam-total-time">Tổng thời lượng: <span>{this.props.totalTime} phút</span></h5>
                            <div className="content-part">
                                <div className="bar-progress"></div>
                                <div className="sub-part start part-item">
                                    <div className="icon">
                                        <i aria-hidden="true" className="fa fa-circle"></i>
                                    </div>
                                    <div className="name-part"><h5>Bắt đầu</h5></div>
                                </div>

                                <ExamTestDetailLanguagePart currentPart={this.props.currentPart}
                                    partIndex={ConstantCode.EXAM_TEST_PART_CODE.PART1}
                                    title={'Từ vựng - Kanji - Ngữ pháp'}
                                    partDuration={this.props.part1Duration}
                                    secondLeft={this.props.secondLeft} />

                                <ExamTestDetailLanguagePart currentPart={this.props.currentPart}
                                    partIndex={ConstantCode.EXAM_TEST_PART_CODE.PART2}
                                    title={'Đọc hiểu'}
                                    partDuration={this.props.part2Duration}
                                    secondLeft={this.props.secondLeft} />

                                <ExamTestDetailLanguagePart currentPart={this.props.currentPart}
                                    partIndex={ConstantCode.EXAM_TEST_PART_CODE.PART3}
                                    title={'Nghe hiểu'}
                                    partDuration={this.props.part3Duration}
                                    secondLeft={this.props.secondLeft} />

                                <ExamTestDetailSubPart currentPart={this.props.currentPart}
                                    partIndex={ConstantCode.EXAM_TEST_PART_CODE.REPORT}
                                    type={'report'}
                                    namePart={'Kết quả thi'} />

                                <div className="sub-part finish part-item">
                                    <div className="icon">
                                        <i aria-hidden="true" className="fa fa-circle"></i>
                                    </div>
                                    <div className="name-part"><h5>Kết thúc</h5></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <PerformExamTest />
            </div>
        )
    }
}

export default ExamTestDetail