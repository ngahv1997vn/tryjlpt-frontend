import React, { Component } from 'react';

class ExamTestAnswer extends Component {

    handleClick = () => {
        this.props.parentQuestion.setState(state => ({
            checkedAnswerId: this.props.answerId
        }));

        let parentQuestionProps = this.props.parentQuestion.props;

        parentQuestionProps.checkAnAnswer(
            parentQuestionProps.examTest.answerSheet,
            parentQuestionProps.examTest.status.currentPart,
            parentQuestionProps.questionId,
            this.props.answerId
        )
    }

    render() {
        return (
            <div className="exam-test-answer-item">
                <div className="aw-check">
                    <input type="radio" value="1" className="exam-test-radio-input" readOnly
                        checked={this.props.parentQuestion.state.checkedAnswerId === this.props.answerId ? true : false} />
                    <label className="exam-test-radio-label" onClick={this.handleClick}
                        dangerouslySetInnerHTML={{ __html: `<span>${this.props.answerNumber}.</span>${this.props.answer.content}` }}>
                    </label>
                </div>
            </div>
        )
    }
}

export default ExamTestAnswer;