import React, { Component } from 'react';

class ExamTestDetailSubPart extends Component {

    render() {
        if (this.props.currentPart === this.props.partIndex)
            return (
                <div className={`sub-part ${this.props.type} part-item`}>
                    <div className="icon">
                        <i aria-hidden="true" className="fa fa-circle"></i>
                        <span>
                            <i aria-hidden="true" className="icon-ani fa fa-circle ani-exam"></i>
                            <i aria-hidden="true" className="icon-ani fa fa-circle-thin ani-exam2"></i>
                        </span>
                    </div>
                    <div className="name-part">
                        <h5>{this.props.namePart}</h5>
                    </div>
                </div>
            )

        return (
            <div className={`sub-part ${this.props.type} part-item`}>
                <div className="icon"><i aria-hidden="true" className="fa fa-circle"></i>
                </div>
                <div className="name-part">
                    <h5>{this.props.namePart}</h5>
                </div>
            </div>
        )

    }
}

export default ExamTestDetailSubPart;