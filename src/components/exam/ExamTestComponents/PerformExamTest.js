import React, { Component } from 'react'
import { connect } from 'react-redux'
import { actSubmitExamByPartRequest, actEndExamTest } from '../../../actions/ExamTest'
import * as ConstantCode from '../../../constants/constant-code'
import { withRouter } from 'react-router-dom'

class PerformExamTest extends Component {
    constructor(props) {
        super(props)
        this.isClicked = false
    }

    handleClickNext = () => {
        if (!this.isClicked) {
            this.isClicked = true

            this.props.submitExamByPartRequest(
                this.props.exam,
                this.props.examTest
            )
            
            window.scroll({
                top: 0, 
                left: 0, 
                behavior: 'smooth'
            })
            
            setTimeout(() => {
                this.isClicked = false
            }, 2000)
        }

    }

    handleClickEnd = () => {
        console.log("This props end: ", this.props)
        this.props.history.push(`/exam-detail/${this.props.exam.id}`)
        this.props.endExamTest()
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        if (nextProps.examTest.status.currentPart !== this.props.examTest.status.currentPart)
            this.isClicked = false
    }

    render() {
        console.log("This props render in PerformExamTest")
        if (this.props.examTest.status.currentPart < ConstantCode.EXAM_TEST_PART_CODE.PART3)
            return (
                <div id="btn-perform-exam" onClick={this.handleClickNext}>
                    <div className="btn-cpl-part btn-cplt-part-dt">
                        <a href="javascript:;">
                            Tiếp theo
                        </a>
                    </div>
                </div>
            )

        if (this.props.examTest.status.currentPart === ConstantCode.EXAM_TEST_PART_CODE.REPORT)
            return (
                <div id="btn-perform-exam" onClick={this.handleClickEnd}>
                    <div className="btn-cpl-part btn-cplt-part-dt">
                        <a href="javascript:;">
                            Kết thúc
                        </a>
                    </div>
                </div>
            )

        return (
            <div id="btn-perform-exam" onClick={this.handleClickNext}>
                <div className="btn-cpl-part btn-cplt-part-dt">
                    <a href="javascript:;">
                        Hoàn thành
                    </a>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        exam: state.exam.payload,
        examTest: state.examTest,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        submitExamByPartRequest: (exam, examTest) => {
            dispatch(actSubmitExamByPartRequest(exam, examTest))
        },
        endExamTest: () => {
            dispatch(actEndExamTest())
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(PerformExamTest))