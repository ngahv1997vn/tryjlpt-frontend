import React, { Component } from 'react'

class ExamTestReport extends Component {

    render() {
        let score = 0
        let maxScore = 0

        function filterScore(score) {
            return Math.round(score)
        }

        for (let partResult of this.props.examTestResult) {
            score += partResult.user_part_result
            maxScore += this.props.exam[`${partResult.part}_score`]
        }   
        return (
            <div className="row ExamTest--report">
                <div className="col-md-4">
                    <h3>Tổng điểm</h3>
                    <p>
                        <span className="ExamTest--total-score ExamTest--correct">{filterScore(score)}</span>    
                        <span className="ExamTest--max-score ExamTest--slash-total">/ {maxScore}</span>
                    </p>
                </div>
                <div className="col-md-8">
                    {
                        this.props.examTestResult.map((partResult, key) => {
                            return (
                                <div key={key} className="ExamTest--part-score-block">
                                    <h4><span>Điểm {partResult.part} </span></h4>
                                    <p>
                                        <span className="ExamTest--part-score ExamTest--correct">
                                            {filterScore(partResult.user_part_result)} </span>
                                        / {this.props.exam[`${partResult.part}_score`]}
                                    </p>
                                </div>
                            )   
                        })
                    }
                </div>
            </div>
        )
    }
}

export default ExamTestReport