import React, { Component } from 'react';
import { connect } from 'react-redux'

import { actSubmitExamByPartRequest, actSetExamTestStatus } from 'actions/ExamTest'
import SimpleCountdown from 'components/shared/SimpleCountdown';

class ExamTestDetailLanguagePart extends Component {

    handleCountDownTimeOut = () => {
        this.props.submitExamByPartRequest(
            this.props.exam,
            this.props.examTest
        )
        
        window.scroll({
            top: 0, 
            left: 0, 
            behavior: 'smooth'
        })
    }

    handleSecondLeft = (countdownState) => {
        let currentStatus = this.props.examTest.status
        let status = { ...currentStatus, secondLeft: countdownState.seconds}

        this.props.setExamTestStatus(status)
    }

    render() {
        console.log("This props in render ExamTestDetailLanguagePart", this.props)
        if (this.props.currentPart === this.props.partIndex)
            return (
                <div className="main-part language-part part-item currentPart">
                    <div className="icon">
                        <i aria-hidden="true" className="fa fa-circle"></i>
                        <span>
                            <i aria-hidden="true" className="icon-ani fa fa-circle ani-exam"></i>
                            <i aria-hidden="true" className="icon-ani fa fa-circle-thin ani-exam2"></i>
                        </span>
                    </div>
                    <div className="name-part">
                        <h4>{this.props.title}</h4>
                        <h5>{this.props.partDuration} phút</h5>
                        <SimpleCountdown classProp={"exam-test-countdown"} hour={0} minute={0} second={this.props.secondLeft} 
                            callbackTimeOut={this.handleCountDownTimeOut}
                            callbackPerMinute={this.handleSecondLeft} />
                    </div>
                </div>
            );

        return (
            <div className="main-part language-part part-item">
                <div className="icon"><i aria-hidden="true" className="fa fa-circle"></i></div>
                <div className="name-part">
                    <h4>{this.props.title}</h4>
                    <h5>{this.props.partDuration} phút</h5>
                </div>
            </div>
        )

    }
}

const mapStateToProps = (state) => {
    return {
        exam: state.exam.payload,
        examTest: state.examTest,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        submitExamByPartRequest: (exam, examTest) => {
            dispatch(actSubmitExamByPartRequest(exam, examTest))
        },
        setExamTestStatus: (status) => {
            dispatch(actSetExamTestStatus(status))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ExamTestDetailLanguagePart)
