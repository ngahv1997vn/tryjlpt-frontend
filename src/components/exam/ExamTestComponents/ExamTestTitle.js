import React, { Component } from 'react';

class ExamTestTitle extends Component {

    render() {
        return (
            <h3 className="exam-test-title">
                {this.props.title}
            </h3>
        );
    }
}

export default ExamTestTitle;