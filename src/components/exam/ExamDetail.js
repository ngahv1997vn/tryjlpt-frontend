import React, { Component } from 'react';
import ShikenMondai from '../shared/ShikenMondai';
import AvataItem from '../shared/AvataItem';
import { connect } from 'react-redux';
import { actGetExamToNRequest } from '../../actions/index'
import { actGetExamDetailAPI } from '../../actions/Exam'
import InformationExam from './InformationExam';
import topbar from 'topbar';
import { withRouter } from 'react-router-dom'
import * as Utils from 'helpers/Utils'

class ExamDetail extends Component {
    componentWillMount() {
        topbar.show()
        this.props.getExamByID(this.getId(window.location.pathname));

    }

    componentDidMount() {
    }

    getId = (str) => {
        str = str.split('/');
        let id = str[2];
        return id
    }

    getMaCode = (n) => {
        if (n === '0') {
            return (<div><h3>Code</h3>
                <input type="text" placeholder="Code" className="form-control" /></div>)
        }
    }

    componentDidUpdate(preProps) {
        if (preProps.location.pathname !== window.location.pathname) {
            topbar.show()
            this.props.getExamByID(this.getId(window.location.pathname));
        }
    }

    handleClickStartExamTest = () => {
        console.log("this props handleClickStartExamTest", this.props)
        Utils.removeItemsInLocalStorage('examTest', 'examTestStatus', 'examTestAnswerSheet', 'examTestResult')
        this.props.history.push(`/exam-test/${this.getId(window.location.pathname)}`)
    }

    render() {
        var { exams, exam, isLoading } = this.props
        let id = this.getId(window.location.pathname)
        if (!isLoading) {
            topbar.hide()
        }
        return (
            <div className="container">
                <div className="row" style={{ margin: '30px 0 50px 0' }}>
                    <div className="col-lg-9 img-thumbnail">
                        <h3>{exam.title}</h3>
                        <hr />
                        <div className="row">
                            <div className="col-md-6 col-lg-4 padding-bottom"  >
                                <InformationExam exam={exam} />
                            </div>
                            <div className="col-md-6 col-lg-4 padding-bottom" >
                                <div className="card" style={{ height: '350px' }}>


                                    <div className="card-body ">
                                        <center>
                                            <br />
                                            {this.getMaCode(exam.is_free)}
                                            <br />
                                            <p>Ready for test</p>
                                            <br />
                                            <button className="btn btn-primary btn-lg" style={{ width: '9em' }}
                                                onClick={this.handleClickStartExamTest} >
                                                <i className="fa fa-location-arrow"> </i>  Start now</button>
                                        </center>
                                    </div>

                                </div>
                            </div>
                            <div className="col-md-12 col-lg-4 padding-bottom" >
                                <div className="card" style={{ height: '350px' }}>
                                    <div className="card-body">
                                        <center>
                                            <h4>Information of poster</h4>
                                            <br />
                                            <img src="http://dummyimage.com/130x120.jpg/5fa2dd/ffffff" alt="" />
                                        </center>
                                        <br />
                                        <p>Full name: Nguyen Van A</p>
                                        <p>Birthday: 1/2/2019</p>
                                    </div>

                                </div>
                            </div>

                        </div>
                        <h3>Related exam</h3>
                        <hr />
                        <div className="row">

                            {this.showExam(exams)}

                        </div>
                    </div>
                    <div className="col-lg-3 thumbnail">

                    </div>


                </div>

            </div>
        );
    }
    showExam = (exams) => {
        let result = null;
        if (exams && exams.length > 0) {
            result = exams.map((exam, index) => {
                return (
                    <ShikenMondai title={exam.title}
                        key={index}
                        // minutes={`${exam.minutes} minutes `}
                        minutes="110 minutes"
                        product={exam}
                    >
                        <AvataItem color="color-avata1" />
                        <AvataItem color="color-avata2" />
                        <AvataItem color="color-avata3" />
                        <div className="ava-item">
                            <div className="ava-item ava-number-total" ><span className="number">+<span>{exam.amount_people}</span></span></div>
                        </div>

                    </ShikenMondai>
                )
            })
        }
        return result;
    }
}
const mapStateToProps = (state) => {
    return {
        exams: state.home,
        exam: state.exam.payload,
        isLoading: state.exam.isLoading
    }
}
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        getExamByID: (id) => {
            dispatch(actGetExamDetailAPI(id))
        },
        getExamToN: (n) => {
            dispatch(actGetExamToNRequest(n))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ExamDetail));