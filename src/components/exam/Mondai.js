import React, { Component } from 'react';
import GroupQuestion from './GroupQuestion';

class Mondai extends Component {
    constructor(props) {
        super(props);
        this.state = {
            formValid: false
        }
    }

    convertSentenceToQuestion(sentense = []) {
        let questions = []
        console.log(sentense);

        sentense.map(item => {
            let sts = { sentense_id: item.id, sentense: item.content, image: item.image, audio: item.audio }

            if (item.question && item.question.length) {

                questions = [...questions, ...item.question.map((qs, index) => {
                    if (index === 0)
                        return { ...sts, question_id: qs.id, question: qs.content, choose: qs.choose }
                    return { sentense_id: "", sentense: "", image: "", audio: "", question_id: qs.id, question: qs.content, choose: qs.choose }
                })]
            }

        })
        return questions
    }

    render() {
        let { textMondai, id, numberOfQuestion, numberOfChoose, mondai } = this.props;
        let question = this.convertSentenceToQuestion(mondai && mondai.sentence ? mondai.sentence : [])
        return (
            <div className="card">
                <div className="card-header">
                    <a className="card-link" data-toggle="collapse" href={"#number" + id}>
                        {textMondai}
                    </a>
                </div>
                <div id={"number" + id} className="collapse" data-parent="#accordion">
                    {
                        Array(numberOfQuestion).fill().map((_item, index) => (
                            <GroupQuestion
                                key={id + '' + index}
                                id={id + '' + (index + 1)}
                                numberOfChoose={numberOfChoose}
                                mondaiId={this.props.mondaiId}
                                question={question && question.length ? question[index] : null}
                            />
                        ))
                    }
                </div>
            </div >
        );
    }
}

export default Mondai;