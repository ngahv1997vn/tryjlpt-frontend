import React, { Component } from 'react';
import Kotaeru from './Kotaeru';

class GroupQuestion extends Component {
    constructor(props) {
        super(props);
        this.state = {
            obj: {},
            sentense: '',
            question: '',
            isshowSentence: true
        }
    }

    onShowSentence = () => {
        let { isshowSentence } = this.state;
        this.setState({ isshowSentence: !isshowSentence })
    }

    render() {
        let { numberOfChoose, id, question } = this.props
        let { isshowSentence } = this.state
        question = question ? question : null;
        console.log(question);
        if ((question && question.sentense === '')) {
            isshowSentence = true;
        }
        return (
            <div className="card-body">
                <div className="row" hidden={isshowSentence}>
                    <div className="col-md-2">
                        <b>
                            <i className="fas fa-comments"> </i>
                            <label htmlFor={"sentense" + id}>{'Sentense '} </label>
                        </b>
                    </div>
                    <div className="col-md-10">
                        <input type="text" name={'sentenseId' + id} defaultValue={question && question.sentense_id ? question.sentense_id : ''} hidden />
                        <textarea name={"sentense" + id} rows={3} className="form-control" defaultValue={question && question.sentense ? question.sentense : ''}></textarea>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="col-md-2">
                        <b>
                            <label className="text-success" htmlFor={'question' + id}>Question {id}:</label>
                        </b>
                        <label className="text-primary" style={{ cursor: 'pointer' }} onClick={this.onShowSentence}>{isshowSentence ? 'Show Sentence' : 'Hide Sentence'}</label>
                    </div>
                    <div className="col-md-10">
                        <textarea name={'question' + id} rows={3} className="form-control" defaultValue={question && question.question ? question.question : ''} />
                        <input hidden name={'mondaiId' + id} className="form-control" defaultValue={this.props.mondaiId} />
                        <input hidden name={'questionId' + id} className="form-control" defaultValue={question && question.question ? question.question_id : ''} />
                        <span className="text-danger"></span>
                        <br />
                    </div>
                </div>
                {
                    Array(numberOfChoose).fill().map((_item, index) => (
                        <Kotaeru
                            key={index}
                            number={index}
                            idQuestion={id}
                            id={id + '' + index}
                            choose={question && question.choose ? question.choose[index] : null}
                        />
                    ))
                }
            </div >
        );
    }
}

export default GroupQuestion;