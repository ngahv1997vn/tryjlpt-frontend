import React, { Component } from 'react';
import Mondai from './Mondai';
import { connect } from 'react-redux'
import { actUpdateExamPartAPI } from '../../actions/Exam';
import { Redirect } from 'react-router-dom';
import Loading from '../shared/Loading';

class Part1 extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: {},
            isRedirect: false
        }
    }

    onSubmit = (e) => {
        e.preventDefault()
        let { part1 } = this.props.exam.templates;
        let sentenses = []
        let data = []

        part1.map((item, index) => {
            let questions = []
            Array(item.number_sentence_recommend).fill().map((i, id) => {
                let question = e.target['question' + index + '' + (id + 1)];
                let questionId = e.target['questionId' + index + '' + (id + 1)];
                let sentense = e.target['sentense' + index + '' + (id + 1)];
                let sentenseId = e.target['sentenseId' + index + '' + (id + 1)];
                let mondaiId = e.target['mondaiId' + index + '' + (id + 1)];
                let chooses = []
                Array(4).fill().map((value, idq) => {
                    let answer = e.target['answer' + index + '' + (id + 1) + '' + idq];
                    let choose = e.target['choose' + index + '' + (id + 1)];
                    let chooseId = e.target['chooseId' + index + '' + (id + 1) + '' + idq];
                    console.log(chooseId, chooseId.value);

                    chooses.push({ id: chooseId.value, content: answer.value, is_correct: choose[idq].checked ? 1 : 0 });
                })
                questions.push({ sentenseId: sentenseId.value, questionId: questionId.value, sentense: sentense.value, mondaiId: mondaiId.value, content: question.value, choose: chooses });
            })
            sentenses.push(questions)
        })
        console.log(sentenses);

        sentenses = sentenses.map(item => {
            item.map((element, id) => {
                if (id === 0) {
                    data.push({ id: element.sentenseId, content: element.sentense, mondai_id: element.mondaiId, question: [{ id: element.questionId, content: element.content, choose: element.choose }] })
                }
                if (element.sentense !== "" && id !== 0) {
                    data.push({ id: element.sentenseId, content: element.sentense, mondai_id: element.mondaiId, question: [{ id: element.questionId, content: element.content, choose: element.choose }] })
                }
                if (element.sentense === "" && id !== 0) {
                    data[data.length - 1].question.push({ id: element.questionId.value, content: element.content, choose: element.choose })
                }
            })
        })
        this.props.onSave(data, this.props.exam.id)
        this.setState({ isRedirect: true })
    }


    render() {
        if (!Object.keys(this.props.exam).length)
            return <Redirect to="/exam/create" />
        let { templates } = this.props.exam
        let { part1 } = templates ? templates : { part1: null };
        let { exam, isLoading, parts } = this.props;
        let { isRedirect } = this.state;
        console.log(parts);

        if (exam.part1 && Object.keys(exam.part1).length && isRedirect) {
            return <Redirect to='/exam/part2' />
        }
        let part = parts && parts.length ? parts[0] : null
        console.log(part);
        if (part && Object.keys(part).length && isRedirect && !isLoading) {
            return <Redirect to='/exam/part2' />
        }
        return (
            <div className="col-md-12">
                <div id="accordion">
                    <form onSubmit={this.onSubmit}>
                        {part1 ? part1.map((_item, index) =>
                            (
                                <Mondai
                                    key={_item.id}
                                    id={index}
                                    mondai={part ? part.mondais[index] : null}
                                    mondaiId={_item.id}
                                    numberOfQuestion={_item.number_sentence_recommend}
                                    numberOfChoose={_item.number_choose_recommend}
                                    textMondai={_item.content}
                                />
                            )
                        ) : <Redirect to='/exam/create' />}
                        <br />
                        <div className="row">
                            <div className="col-md-3"></div>
                            <div className="col-md-6">
                                <button type="submit"
                                    disabled={isLoading}
                                    className="btn btn-success">
                                    <i className="fa fa-arrow-right"></i>
                                    Next</button>
                                <Loading isLoading={isLoading} />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    exam: state.exam.payload,
    isLoading: state.exam.isLoading,
    parts: state.exam.payload.parts
})

const mapDispatchToProps = (dispatch, props) => {
    return {
        onSave: (exam, id) => dispatch(actUpdateExamPartAPI(exam, id))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Part1);