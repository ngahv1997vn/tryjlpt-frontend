import React, { Component } from 'react';

class Kotaeru extends Component {
    render() {
        let { id, number, idQuestion, choose } = this.props;
        return (
            <div className="row">
                <div className="col-md-2">
                </div>
                <div className="col-md-10">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="input-group mb-3">
                                <div className="input-group-prepend">
                                    <label className="input-group-text" htmlFor={'choose' + id + number}>{(number + 1)}</label>
                                    <span className="input-group-text">
                                        <input id={'choose' + id} name={'choose' + idQuestion} type="radio" defaultChecked={choose && choose.is_correct === 1} />
                                    </span>
                                </div>
                                <input type="text" name={'chooseId' + id} defaultValue={choose && choose.id ? choose.id : ''} hidden />

                                <input name={"answer" + id} type="text" className="form-control" defaultValue={choose && choose.content ? choose.content : ''} />

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Kotaeru;