import React, { Component } from 'react';
import Header from '../shared/Header';
import TransferCircle from '../shared/TransferCircle';
import Part from './Part0';
import Part1 from './Part1';
import Part2 from './Part2';
import { connect } from 'react-redux';
import { Route } from "react-router-dom";
import topbar from 'topbar';

class CreateExam extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: 0,
            data: {}
        }
    }

    componentWillMount() {
        topbar.show()
    }

    showId = (id) => {
        this.setState({ id })
    }

    render() {
        let { part1, part2, part3 } = this.props.exam;
        let { exam, isLoading } = this.props;
        if (!isLoading) {
            topbar.hide()
        }
        return (
            <div className="container" style={{ marginTop: '30px', marginBottom: '30px' }}>
                <div className=" row">
                    <div className="col-md-12">
                        {
                            Object.keys(exam).length ?
                                <Header title="Update Exam" /> :
                                <Header title="Create Exam" />
                        }

                        <TransferCircle pathname={this.props.location.pathname} part={{ part1, part2, part3 }} />
                        <Route
                            exact
                            path={'/exam/create'}
                            component={Part}
                        />
                        <Route
                            path={'/exam/part1'}
                            component={Part1}
                        />
                        <Route
                            path={'/exam/part2'}
                            component={Part2}
                        />
                    </div>
                </div >
            </div>
        );
    }
}
const mapStateToProps = (state) => ({
    exam: state.exam.payload,
    isPart: state.exam,
    isLoading: state.exam.isLoading
})
export default connect(mapStateToProps, null)(CreateExam);