import React, { Component } from 'react';

class Sentense extends Component {
    render() {
        let { number } = this.props
        return (
            <div className="row">
                <div className="col-md-2">
                    <label htmlFor="sentense1">{'Sentense' + number}: </label>
                </div>
                <div className="col-md-10">
                    <div id="froala-editor" />
                    <textarea name="sentense1" rows="3" className="form-control"></textarea>
                </div>
            </div>
        );
    }
}

export default Sentense;