import React, { Component } from 'react';
import { connect } from 'react-redux';
import Mondai from './Mondai';
import { actUpdateExamPartAPI2 } from '../../actions/Exam';
import { Redirect } from 'react-router-dom';
import Loading from '../shared/Loading';
import swal from 'sweetalert';

class Part2 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isRedirect: false
        }
    }

    onSubmit = (e) => {
        e.preventDefault()
        let { part2 } = this.props.exam.templates;
        let sentenses = []
        let data = []

        part2.map((item, index) => {
            let questions = []
            Array(item.number_sentence_recommend).fill().map((i, id) => {
                let question = e.target['question' + index + '' + (id + 1)];
                let questionId = e.target['questionId' + index + '' + (id + 1)];
                let sentense = e.target['sentense' + index + '' + (id + 1)];
                let sentenseId = e.target['sentenseId' + index + '' + (id + 1)];
                let mondaiId = e.target['mondaiId' + index + '' + (id + 1)];
                let chooses = []
                console.log(id)
                Array(4).fill().map((value, idq) => {

                    let answer = e.target['answer' + index + '' + (id + 1) + '' + idq];
                    // let answerId = e.target['answerId' + index + '' + (id + 1) + '' + idq];

                    let choose = e.target['choose' + index + '' + (id + 1)];
                    let chooseId = e.target['chooseId' + index + '' + (id + 1) + '' + idq];
                    console.log(choose);

                    chooses.push({ id: chooseId && chooseId.value ? chooseId.value : '', content: answer && answer.value ? answer.value : '', is_correct: choose[idq].checked ? 1 : 0 });
                })
                questions.push({ sentenseId: sentenseId.value, questionId: questionId.value, sentense: sentense.value, mondaiId: mondaiId.value, content: question.value, choose: chooses });
            })
            sentenses.push(questions)
        })
        sentenses = sentenses.map(item => {
            item.map((element, id) => {
                if (id === 0) {
                    data.push({ id: element.sentenseId, content: element.sentense, mondai_id: element.mondaiId, question: [{ id: element.questionId, content: element.content, choose: element.choose }] })
                }
                if (element.sentense !== "" && id !== 0) {
                    data.push({ id: element.sentenseId, content: element.sentense, mondai_id: element.mondaiId, question: [{ id: element.questionId, content: element.content, choose: element.choose }] })
                }
                if (element.sentense === "" && id !== 0) {
                    data[data.length - 1].question.push({ id: element.questionId, content: element.content, choose: element.choose })
                }
            })
        })
        this.props.onSave(data, this.props.exam.id)
        this.setState({ isRedirect: true })
    }


    render() {
        if (!Object.keys(this.props.exam).length)
            return <Redirect to="/exam/create" />
        let { part2 } = this.props.template;
        let { isLoading, parts,exam } = this.props;
        let part = parts && parts.length ? parts[1] : null
        console.log(this.props.template);

        if(exam && exam.part2 && Object.keys(exam.part2).length && !isLoading ){
            swal("Save success!","","success");
            return <Redirect to="/my-exam" />
        }
        return (
            <div className="col-md-12">
                <div id="accordion">
                    <form onSubmit={this.onSubmit}>
                        {part2.map((_item, index) =>
                            (
                                <Mondai
                                    key={_item.id}
                                    id={index}
                                    mondaiId={_item.id}
                                    numberOfQuestion={_item.number_sentence_recommend}
                                    numberOfChoose={_item.number_choose_recommend}
                                    textMondai={_item.content}
                                    mondai={part ? part.mondais[index] : null}
                                />
                            )
                        )}
                        <br />
                        <div className="row">
                            <div className="col-md-3"></div>
                            <div className="col-md-6">
                                <button type="submit"
                                    disabled={isLoading}
                                    className="btn btn-success">
                                    <i className="fa fa-arrow-right"></i>
                                    Next</button>
                                <Loading isLoading={isLoading} />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    template: state.exam.payload.templates,
    exam: state.exam.payload,
    isLoading: state.exam.isLoading,
    parts: state.exam.payload.parts
})

const mapDispatchToProps = (dispatch, props) => {
    return {
        onSave: (exam, id) => dispatch(actUpdateExamPartAPI2(exam, id))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Part2);