import React, { Component } from 'react';
import { connect } from 'react-redux';
import { actStartExamRequest } from '../../actions/ExamTest'
import { actSetRedirectTo } from '../../actions/SignIn'
import * as ConstantCode from '../../constants/constant-code'
import ExamTestMondai from './ExamTestComponents/ExamTestMondai';
import ExamTestDetail from './ExamTestComponents/ExamTestDetail';
import ExamTestReport from './ExamTestComponents/ExamTestReport';
import * as HelperModal from '../../helpers/Modal'
import ExamTestTitle from 'components/exam/ExamTestComponents/ExamTestTitle';

class ExamTest extends Component {
    componentDidMount() {

        if (!this.props.isPublisher && !this.props.isUser) {
            this.props.setRedirectTo(this.props.location.pathname)
            HelperModal.ShowModal("#modal-signin")
            return 
        }

        this.props.startExam(this.props.match.params.id);
    }

    render() {
        console.log("Log this props in render ExamTest: ", this.props);
        if (!this.props.isLoading)
            return <div></div>

        let totalTime = this.props.exam.examDurationSetting.part1_duration +
        this.props.exam.examDurationSetting.part2_duration +
        this.props.exam.examDurationSetting.part3_duration

        if (this.props.examTest.status.currentPart < ConstantCode.EXAM_TEST_PART_CODE.REPORT) {
            
        
            let partTitle = this.props.format[this.props.exam.level]
                            [this.props.exam.parts[this.props.examTest.status.currentPart - 1].part]
                            .title

            return (
                <div className="container">
                    <div className="row">
                        <div className="col-md-4">
                            <ExamTestDetail title={this.props.exam.title}
                                totalTime={totalTime}
                                currentPart={this.props.examTest.status.currentPart}
                                part1Duration={this.props.exam.examDurationSetting.part1_duration}
                                part2Duration={this.props.exam.examDurationSetting.part2_duration}
                                part3Duration={this.props.exam.examDurationSetting.part3_duration}
                                secondLeft={this.props.examTest.status.secondLeft} />
                        </div>
                        <div className="col-md-8 ExamTest--content">
                            <ExamTestTitle title={partTitle} />
                            {
                                this.props.exam.parts[this.props.examTest.status.currentPart - 1].mondais.map((mondai, index) => {
                                    return (
                                        <ExamTestMondai key={mondai.id} mondaiNumber={index + 1} mondai={mondai} />
                                    )
                                })     
                            }
                        </div>
                    </div>
                </div>
            );    
        } 

        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-4">
                        <ExamTestDetail title={this.props.exam.title}
                            totalTime={totalTime}
                            currentPart={this.props.examTest.status.currentPart}
                            part1Duration={this.props.exam.examDurationSetting.part1_duration}
                            part2Duration={this.props.exam.examDurationSetting.part2_duration}
                            part3Duration={this.props.exam.examDurationSetting.part3_duration}
                            secondLeft={this.props.examTest.status.secondLeft} />
                    </div>
                    <div className="col-md-8 ExamTest--content">                   
                        <ExamTestReport examTestResult={this.props.examTest.result}
                            exam={this.props.exam} />
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        isPublisher: state.signin.isPublisher,
        isUser: state.signin.isUser,
        exam: state.exam.payload,
        isLoading: state.examTest.isLoading,
        examTest: state.examTest,
        format: state.examTest.format
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        startExam: (id) => {
            dispatch(actStartExamRequest(id))
        },
        setRedirectTo: (redirectTo) => {
            dispatch(actSetRedirectTo(redirectTo))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ExamTest);