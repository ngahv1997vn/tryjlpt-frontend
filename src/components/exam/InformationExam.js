import React, { Component } from 'react';

class InformationExam extends Component {
    render() {
        let { exam } = this.props;
        let minutes = parseInt(exam.part1_score) + parseInt(exam.part2_score) + parseInt(exam.part3_score);
        return (
            <div className="card" style={{ height: '350px' }}>
                <div className="card-body">
                    <h5>Duration: {minutes} minutes </h5>
                    <table>
                        <tbody>
                            <tr>
                                <td></td><td>Start</td>
                            </tr>
                            <tr>
                                <td><i className="fa fa-lightbulb"></i></td
                                ><td><b>Vocabulary, kanji, grammar</b></td>
                            </tr>
                            <tr>
                                <td></td><td>{exam.part1_score} minutes</td>
                            </tr>
                            <tr>
                                <td></td><td>Take a break</td>
                            </tr>
                            <tr>
                                <td></td><td>5 minutes</td>
                            </tr>
                            <tr>
                                <td><i className="fa fa-book-reader">&nbsp;</i></td>
                                <td><b>Reading</b></td>
                            </tr>
                            <tr>
                                <td></td><td>{exam.part2_score} minutes</td>
                            </tr>
                            <tr>
                                <td><i className="fa fa-headphones">&nbsp;</i></td>
                                <td><b>Listening</b></td>
                            </tr>
                            <tr>
                                <td></td><td>{exam.part3_score} minutes</td>
                            </tr>
                            <tr>
                                <td></td><td> Finish</td>
                            </tr>
                        </tbody>
                    </table>

                </div>

            </div>
        );
    }
}

export default InformationExam;