import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { actCheckLogin } from '../../actions';
import { actSignOutServer } from '../../actions/LogOut'

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isRedirect: false
        }
    }

    onLogout = () => {
        this.setState({ isRedirect: true })
        this.props.checkLogout();


    }

    componentDidMount() {
        this.props.checkLogin()
    }

    setRedirect = () => {
        if (this.state.isRedirect) {
            this.setState({
                isRedirect: false
            });
            return (< Redirect to="/" />)
        }
    }

    render() {
        let { isAuthentication } = this.props.signin;
        let { isPublisher, isUser } = this.props
        console.log(this.props.isPublisher);

        return (
            < div >
                <nav className="navbar navbar-expand-lg navbar-dark bg-dark ">
                    <div className="container">
                        <Link className="navbar-brand" to="/"><b>TryJLPT</b></Link>
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse" id="navbarNav">
                            <ul className="navbar-nav">
                                <li className="nav-item" hidden={!isUser}>
                                    <Link to='/my-profile' className="nav-link" style={{ textDecoration: 'none' }}>
                                        Profile
                                    </Link>
                                </li>
                                {/* <li className="nav-item " >
                                    <Link to='/exam' className="nav-link" style={{ textDecoration: 'none' }}>
                                        My Exam
                                    </Link>
                                </li> */}
                                <li className="nav-item " hidden={!isPublisher}>
                                    <Link to='/my-exam' className="nav-link" style={{ textDecoration: 'none' }}>
                                        My Exam
                                    </Link>
                                </li>
                            </ul>
                        </div>
                        {this.setRedirect()}
                        <button hidden={isAuthentication}
                            className="btn btn-primary"
                            type="button"
                            data-toggle="modal"
                            data-target="#modal-signin">
                            Sign In
                        </button>
                        <button hidden={!isAuthentication}
                            className="btn btn-danger"
                            type="button"
                            onClick={this.onLogout}>
                            Logout
                        </button>
                    </div>
                </nav>
            </div >
        );
    }
}

const mapStateToProps = state => ({
    signin: state.signin,
    isPublisher: state.signin.isPublisher,
    isLoading: state.signin.isLoading,
    isUser: state.signin.isUser
})

const mapDispatchToProps = (dispatch, props) => {
    return {
        checkLogin: () => {
            dispatch(actCheckLogin())
        },
        checkLogout: () => {
            dispatch(actSignOutServer())
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);