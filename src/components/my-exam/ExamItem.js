import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { actGetExamRequest } from '../../actions/Exam'
import swal from "sweetalert";
import topbar from 'topbar';

class ExamItem extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isRedirect: false
        }
    }

    onDelete = (id) => {
        swal({
            title: "Are you sure you want to delete?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    swal("Delete success!", {
                        icon: "success",
                    });
                    this.props.onDelete(id)
                }
            });
    }

    render() {
        let { index, exam, exams, isLoading } = this.props
        let { isRedirect } = this.state

        if (isLoading)
            topbar.show()
        else
            topbar.hide()

        let freeName = exam.is_free === "1" ? 'Free' : 'not Free';
        let freeClass = exam.is_free === "1" ? 'primary' : 'secondary';
        let privateClass = exam.is_private === "1" ? 'fa fa-check text-success' : 'fa fa-times text-danger';
        let statusClass = exam.publishing_status === "approved" ? 'warning' : 'dark';

        if (Object.keys(exams).length && isRedirect && !isLoading) {
            return <Redirect to="/exam/create" />
        }

        return (
            <tr>
                <td className="pt-4">{index + 1}</td>
                <td className="pt-4">{exam.title}</td>
                <td className="pt-4">{exam.level}</td>
                <td><li className={`m-3 ${privateClass}`}></li></td>
                <td>
                    <samp className={`badge badge-${freeClass} p-2 m-3`}>{freeName}</samp>
                </td>
                <td><samp className={`badge badge-${statusClass} p-2 m-3`}>{exam.publishing_status}</samp></td>
                <td>
                    <button
                        className="btn btn-success" onClick={() => {
                            this.props.showExambyId(exam.id);
                            this.setState({ isRedirect: true })
                        }}
                    ><li className="fa big-icon fa-edit"></li>Edit</button>
                    <button type="button" className="btn btn-danger m-2" onClick={() => { this.onDelete(exam.id) }} ><i className=" fa fa-trash-alt" ></i>Delete</button>
                </td>
            </tr>
        );
    }
}

const mapStateToProps = (state) => ({
    exams: state.exam.payload,
    isLoading: state.exam.isLoading
})

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        showExambyId: (id) => {
            dispatch(actGetExamRequest(id))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ExamItem)