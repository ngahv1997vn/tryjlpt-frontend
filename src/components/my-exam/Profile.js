import React, { Component } from 'react';
import topbar from 'topbar';

class Profile extends Component {
    componentWillMount() {
        topbar.show()
    }
    componentDidMount() {
        topbar.hide()
    }
    render() {
        return (
            <div className='container' >
                <div className='row img-thumbnail' style={{ boxShadow: "0 2px 4px rgba(0, 0, 0, .18)", margin: "20px 100px" }}>
                    <div className='col-md-4 m-4'>
                        <img className="img-file" src="http://dummyimage.com/229x225.jpg/5fa2dd/ffffff" alt="" />
                    </div>
                    <div className='col-md-6'>
                        <div className="right-content ml-5">
                            <span className="say-hi">HELLO</span>
                            <h2 className="my-name">
                                I'm <span>Nguyễn Văn A </span>
                            </h2>
                            <span className="my-website">
                                http::tryjlpt.com
                                    <div className="details">
                                    <div className="labels">
                                        <p>Name</p>
                                        <p>Company</p>
                                        <p>Job</p>
                                        <p>Age</p>
                                    </div>
                                    <div className="information">
                                        <p>NGUYỄN VĂN A</p>
                                        <p>GMO-Z.com</p>
                                        <p>Guard</p>
                                        <p>20 Years Old</p>
                                    </div>
                                </div>
                            </span>
                            <button type="submit" className="btn btn-info btn-fill pull-right">Update Profile</button>
                        </div>
                    </div>

                </div>

            </div>

        );
    }
}

export default Profile;