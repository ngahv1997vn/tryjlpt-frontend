import React, { Component } from 'react';

class ExamList extends Component {
    render() {
        return (
            <div className="panel-card" style={{ minHeight: "380px" }}>
                <center><h3 className="panel-title"><b>EXAM CREATED</b></h3></center>
                <table className="table table-inverse table-hover">
                    <thead className="">
                        <tr>
                            <th>STT</th>
                            <th>Title</th>
                            <th>Level</th>
                            <th>Private</th>
                            <th>Free Exam</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.children}
                    </tbody>
                </table>
            </div>



        );
    }
}

export default ExamList;