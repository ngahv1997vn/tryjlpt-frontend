import React, { Component } from 'react';
import ExamList from './ExamList';
import ExamItem from './ExamItem';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { actShowExamUserCreateRequest, actOnCreateExamLocal, actDeleteExamRequest } from '../../actions/Exam'
import topbar from 'topbar';

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isRedirect: false
        }
    }

    componentWillMount() {

        topbar.show();

        this.props.ShowExams();
    }

    componentDidMount() {
    }

    onDelete = (id) => {
        this.props.onDeleteExam(id);
    }

    onCreateExam = () => {
        this.props.onCreate();
        this.setState({ isRedirect: true })
    }

    render() {
        let { exams, exam, isLoading } = this.props
        let { isRedirect } = this.state
        if (isRedirect && !Object.keys(exam).length) {
            return <Redirect to="/exam/create" />
        }
        exams = exams && exams.length ? exams : []
        if (!isLoading) {
            topbar.hide()
        }
        return (
            <div className="container">
                <div className="row" style={{ margin: '30px 0 50px 0' }}>
                    <div className="col-12" style={{ minHeight: "100%" }}>
                        <div className="panel panel-primary rounded">
                            <div className="card">
                                <div className="card-header">
                                    <div className="row">
                                        <button className="btn btn-primary" onClick={this.onCreateExam}>
                                            <i className="fa fa-plus-square"></i> <span className="font-weight-bold mb-0">create exam</span>
                                        </button>
                                    </div>
                                </div>
                                <ExamList>
                                    {
                                        exams && exams.length ?
                                            (exams.map((exam, index) =>
                                                <ExamItem key={index}
                                                    exam={exam}
                                                    index={index}
                                                    onDelete={this.onDelete}
                                                >
                                                </ExamItem>
                                            )) :
                                            <tr
                                                className="bg-light">
                                                <td colSpan="7">
                                                    <center>you have not created exam  </center>
                                                </td>
                                            </tr>
                                    }
                                </ExamList>
                            </div>
                        </div>
                    </div>

                </div>
            </div >
        );
    }

}
const mapStateToProps = (state) => {
    return {
        exams: state.examuser.payload,
        exam: state.exam.payload,
        isLoading: state.exam.isLoading
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        ShowExams: () => { ///
            dispatch(actShowExamUserCreateRequest())
        },
        onCreate: () => {
            dispatch(actOnCreateExamLocal())
        },
        onDeleteExam: (id) => {
            dispatch(actDeleteExamRequest(id))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Home)