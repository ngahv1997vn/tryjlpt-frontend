import * as Action from '../constants/index';
import * as HelperModal from '../helpers/Modal';
import swal from "sweetalert";
// import nprogress from 'nprogress'
const initialState = {
    payload: {},
    isLoading: false,
    error: {},
    isPublisher: false,
    isUser: false,
    redirectTo: null
}
const publisher = ['PUBLISHER', 'ADMIN']
const user = ['PUBLISHER', 'ADMIN', 'COMMON_USER']
let parseJwt = (token) => {
    if (token) {
        var base64Url = token.split('.')[1];
        var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
        return JSON.parse(window.atob(base64));
    }
    return ''
};

const signin = (state = initialState, action) => {
    switch (action.type) {
        case Action.ON_SIGNIN:
            return { ...state, isLoading: true }
        case Action.SIGNIN_SUCCESS:
            if (!action.result) {
                swal("Login fail!", '', 'error')
                return { ...state, isLoading: false }
            }
            if (action.result && action.result.data.data.access_token) {
                localStorage.setItem("token", action.result.data.data.access_token);
                HelperModal.CloseModal("#modal-signin");
                swal("Login success!", "", "success");

                setTimeout(() => {
                    if (state.redirectTo)
                        window.location.pathname = state.redirectTo
                }, 2000)

                return { ...state, isAuthentication: true }
                // window.location.reload();
            }
            return { ...state, isLoading: false };
        case Action.SIGNIN_SET_REDIRECT:
            return  { ...state, redirectTo: action.redirectTo }
        case Action.SIGNIN_ERROR:
            state = initialState
            state.error = { message: "Incorrect username or password" }
            return { ...state, isLoading: false }
        case Action.ISAUTH:
            if (localStorage.getItem("token")) {
                return { ...state, isAuthentication: true, isLoading: false }
            }
            return { ...state, isAuthentication: false, isLoading: false }
        case Action.LOGOUT_SUCCESS:
            swal("Logout success!", "", "success");
            localStorage.clear();
            return { ...state, isAuthentication: false, isLoading: false }
        case Action.IS_PUBLISHER:
            let tokens = localStorage.getItem('token');
            let roles = parseJwt(tokens).role;
            console.log(roles);

            return { ...state, isLoading: false, isPublisher: publisher.includes(roles), isUser: user.includes(roles) };
        default:
            let token = localStorage.getItem('token');
            let role = parseJwt(token).role;
            return { ...state, isLoading: false, isPublisher: publisher.includes(role), isUser: user.includes(role) };
    }
}

export default signin