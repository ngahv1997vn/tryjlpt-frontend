import * as Action from '../constants/index';
import swal from "sweetalert";

const initialState = {
    payload: {},
    isLoading: false,
    error: {},
    isRedirect: false
}

const signup = (state = initialState, action) => {
    switch (action.type) {
        case Action.SIGNUP:
            return { ...state, isLoading: true }
        case Action.SIGNUP_ERROR:
            swal(action.result[0])
            return { ...state, isLoading: false }
        case Action.SIGNUP_SUCCESS:
            swal("register success");
            state.payload = { data: action.result }
            return { ...state, isLoading: false }
        default:
            return { ...state, payload: {}, isLoading: false };
    }
}
export default signup