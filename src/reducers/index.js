import { combineReducers } from 'redux'
import exam from './Exam'
import home from './Home'
import signin from './SignIn'
import signup from './SignUp'
import examuser from './ExamUser'
import examTest from './ExamTest'

export default combineReducers({
    exam,
    home,
    signin,
    signup,
    examuser,
    examTest,
})