import * as Action from '../constants/index';
const InitialState = {
    payload: {},
    isLoading: false,
    error: {}
}
var findIndex = (exams, id) => {
    var result = -1;
    exams.forEach((exam, index) => {
        if (exam.id === id) {
            console.log(id)
            result = index;
        }
    });
    return result;
}

const exams = (state = InitialState, action) => {
    var index = -1;
    switch (action.type) {
        case Action.SHOW_EXAM_OF_USER:
            state.payload = action.exams
            return { ...state, isLoading: true }
        case Action.DELETE_EXAM_ID:
            index = findIndex(state.payload, action.id);
            state.payload.splice(index, 1);
            return { ...state, isLoading: true }
        default:
            return state
    }
}
export default exams