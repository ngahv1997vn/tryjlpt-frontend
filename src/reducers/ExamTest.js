import * as Action from '../constants/index'
import * as ConstantCode from '../constants/constant-code'

const nameInitialState = {
    payload: {},
    isLoading: false,
    status: {
        currentPart: ConstantCode.EXAM_TEST_PART_CODE.BEGIN,
        secondLeft: null
    },
    answerSheet: {},
    result: [],
    format: {
        'N1': {
            part1: {
                title: '言語知識（もじ・ごい）・文法・読解'
            },
            part2: {
                title: '聴解'
            }
        }, 
        'N2': {
            part1: {
                title: '言語知識（もじ・ごい）・文法・読解'
            },
            part2: {
                title: '聴解'
            }
        },
        'N3': {
            part1: {
                title: '言語知識（もじ・ごい)'
            },
            part2: {
                title: '言語知識（文法）・読解'
            },
            part3: {
                title: '聴解'
            }
        },
        'N4': {
            part1: {
                title: '言語知識（もじ・ごい)'
            },
            part2: {
                title: '言語知識（文法）・読解'
            },
            part3: {
                title: '聴解'
            }
        },
        'N5': {
            part1: {
                title: '言語知識（もじ・ごい)'
            },
            part2: {
                title: '言語知識（文法）・読解'
            },
            part3: {
                title: '聴解'
            }
        },
    }
}

const examTest = (state = nameInitialState, action) => {
    switch (action.type) {
        case Action.START_EXAM_TEST:
            return Object.assign({}, state, {
                payload: action.payload,
                isLoading: true,
                status: action.status,
                answerSheet: action.answerSheet,
                result: action.result
            })
        case Action.END_EXAM_TEST:
            localStorage.removeItem('examTest')
            localStorage.removeItem('examTestAnswerSheet')
            localStorage.removeItem('examTestResult')
            localStorage.removeItem('examTestStatus')
            return nameInitialState
        case Action.SET_EXAM_TEST_STATUS:
            return Object.assign({}, state, {
                status: action.status
            })
        case Action.SET_EXAM_TEST_ANSWER_SHEET:
            return Object.assign({}, state, {
                answerSheet: action.answerSheet
            })
        case Action.PUSH_EXAM_TEST_RESULT:

            let nextState = Object.assign({}, state, {
                result: [...state.result, action.result]
            })

            localStorage.setItem('examTestResult', JSON.stringify(nextState.result))
            return nextState
        default:
            return state
    }
}

export default examTest
