import * as Action from '../constants/index';

const initialState = {
    payload: {},
    isLoading: false,
    error: {}
}

const exam = (state = initialState, action) => {
    switch (action.type) {
        case Action.CREATE_EXAM:
            return { ...state, isLoading: true }
        case Action.ON_CREATE_EXAM_LOCAL:
            return { ...state, payload: {}, isLoading: false }
        case Action.CREATE_EXAM_SUCCESS:
            let parts = state.payload.parts;
            Object.assign(initialState, state)
            state.payload = action.result.data
            state.payload.parts = parts
            return { ...state, isLoading: true }
        case Action.GET_EXAM_ID:
            Object.assign(initialState, state)
            state.payload = action.exam
            return { ...state, isLoading: true }
        case Action.GET_EXAM_DETAIL:
            Object.assign(initialState, state)
            state.payload = action.exam
            return { ...state, isLoading: false }
        case Action.CREATE_EXAM_ERROR:
            Object.assign(initialState, state)
            state.error = action.result
            return { ...state, isLoading: false }
        case Action.GET_MONDAI_BY_LEVEL:
            state.payload.templates = action.result.data
            Object.assign(initialState, state)
            return { ...state, isLoading: false, isPart1: true }
        case Action.UPDATE_EXAM_PART1:
            let templates = state.payload.templates;
            state.payload = action.exam.data.data;
            state.payload.templates = templates;
            Object.assign({ ...state, payload: state.payload }, state)
            return { ...state, isLoading: false }
        case Action.UPDATE_EXAM_PART2:
            state.payload.part2 = action.exam.data
            Object.assign({ ...state, payload: state.payload }, state)
            return { ...state, isLoading: false }
        case Action.GET_EXAM_ID:
            state.payload = action.exam
            return { ...state, isLoading: true }
        default:
            console.log(state);
            return state
    }
}

export default exam