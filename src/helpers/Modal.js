export const CloseModal = idModal => {
    window.$(idModal).modal('hide');
}

export const ShowModal = idModal => {
    window.$(idModal).modal('show');
}