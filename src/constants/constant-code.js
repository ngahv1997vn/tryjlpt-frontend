export const EXAM_TEST_PART_CODE = {
    BEGIN: 0,
    PART1: 1,
    PART2: 2,
    PART3: 3,
    REPORT: 4,
    END: 5
}