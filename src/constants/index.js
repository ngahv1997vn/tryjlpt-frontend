export const SHOW = 'SHOW'
// Constants sign in
export const SIGNIN = 'SIGNIN';
export const ON_SIGNIN = 'ON_SIGNIN';
export const SIGNIN_SUCCESS = 'SIGNIN_SUCCESS';
export const SIGNIN_SET_REDIRECT = 'SIGNIN_SET_REDIRECT';
export const SIGNIN_ERROR = 'SIGNIN_ERROR';
export const IS_PUBLISHER = 'IS_PUBLISHER';
// Constants sign up
export const SIGNUP = 'SIGNUP';
export const SIGNUP_SUCCESS = 'SIGNUP_SUCCESS';
export const SIGNUP_ERROR = 'SIGNUP_ERROR';
export const SHOW_EXAM = 'SHOW_EXAM';
export const INIT_SIGNUP = 'INIT_SIGNUP';
export const ISAUTH = 'ISAUTH';
// Constants Exam
export const CREATE_EXAM = 'CREATE_EXAM'
export const CREATE_EXAM_SUCCESS = 'CREATE_EXAM_SUCCESS'
export const CREATE_EXAM_ERROR = 'CREATE_EXAM_ERROR'
export const SHOW_EXAM_OF_USER = 'SHOW_EXAM_OF_USER'
export const UPDATE_EXAM_PART1 = 'UPDATE_EXAM_PART1'
export const UPDATE_EXAM_PART2 = 'UPDATE_EXAM_PART2'
export const UPDATE_EXAM_PART3 = 'UPDATE_EXAM_PART3'
export const GET_MONDAI_BY_LEVEL = 'GET_MONDAI_BY_LEVEL';
export const ON_CREATE_EXAM_LOCAL = 'ON_CREATE_EXAM_LOCAL';
export const GET_EXAM_ID = 'GET_EXAM_ID'
export const EXAM_TO_N = 'EXAM_TO_N'
export const DELETE_EXAM_ID = 'DELETE_EXAM_ID'
export const GET_EXAM_DETAIL = 'GET_EXAM_DETAIL'
//Constants singn out
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS'
// Constants submit exam
export const START_EXAM_TEST = 'START_EXAM_TEST'
export const END_EXAM_TEST = 'END_EXAM_TEST'
export const SET_EXAM_TEST_STATUS = 'SET_EXAM_TEST_STATUS'
export const SET_EXAM_TEST_ANSWER_SHEET = 'SET_EXAM_TEST_ANSWER_SHEET'
export const PUSH_EXAM_TEST_RESULT = 'PUSH_EXAM_TEST_RESULT'
